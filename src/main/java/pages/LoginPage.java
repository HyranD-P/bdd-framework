package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import tools.SeleniumDriver;

import java.util.Arrays;
import java.util.List;

public class LoginPage extends SeleniumDriver {

    private By loginForm = By.id("login_form");
    private By usernameLocator = By.xpath("//input[@name = 'username']");
    private By passwordLocator = By.xpath("//input[@name = 'password']");
    private By signInButton = By.id("submit_form");


    public LoginPage(String url){
        launchDriver();
        navigate(url);
        //wait for page to load
        wait.until(ExpectedConditions.visibilityOfElementLocated(loginForm));
    }

    public void setUsername(String text){
        wait.until(ExpectedConditions.visibilityOfElementLocated(usernameLocator));
        WebElement userNameTextBox = getDriver().findElement(usernameLocator);
        userNameTextBox.clear();
        userNameTextBox.sendKeys(text);
    }
    public void setPassword(String password){
        wait.until(ExpectedConditions.visibilityOfElementLocated(passwordLocator));
        WebElement passTextBox = getDriver().findElement(passwordLocator);
        passTextBox.clear();
        passTextBox.sendKeys(password);
    }

    public void loginButton(){
        wait.until(ExpectedConditions.elementToBeClickable(signInButton));
        WebElement loginBtn = getDriver().findElement(signInButton);
        loginBtn.click();
    }
    public void validateUserLoggedIn() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text() = 'Welcome ']")));
    }
    public void clickControlDeviceButton(){
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text() = 'Device Control']")));
        WebElement deviceControlBtn = getDriver().findElement(By.xpath("//li[@class = 'icon group_management chosen']"));
        deviceControlBtn.click();
    }

    public void validateSubmenuItem(){
        List<String> list = Arrays.asList("Dashboard",
                "Devices",
                "Computers",
                "Users",
                "Groups",
                "Global Rights",
                "Global Settings",
                "File Allowlist",
                "Custom Classes");
        for (String s : list) {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text() = '" + s + "']")));
        }
    }
    public void selectSubmenuItem(String option){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text() = '" + option + "']")));
        WebElement optionLink = getDriver().findElement(By.xpath("//span[text() = '" + option + "']"));
        optionLink.click();
    }

    public void selectButtonCreate(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='btn epp-btn create']")));
        WebElement createButton = getDriver().findElement(By.xpath("//button[@class='btn epp-btn create']"));
        createButton.click();
    }

    public void selectButtonSave(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='btn epp-btn']")));
        WebElement createButton = getDriver().findElement(By.xpath("//button[@class='btn epp-btn']"));
        createButton.click();
    }

    public void createPage(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[text() = 'Device Information']")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text() = 'Details']")));
    }

    public void enterDeviceDetails(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name = 'clientdevice[name]']")));
        WebElement userNameTextBox = getDriver().findElement(By.xpath("//input[@name = 'clientdevice[name]']"));
        userNameTextBox.clear();
        userNameTextBox.sendKeys("Nova 9");

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name = 'clientdevice[vid]']")));
        WebElement vidTextBox = getDriver().findElement(By.xpath("//input[@name = 'clientdevice[vid]']"));
        vidTextBox.clear();
        vidTextBox.sendKeys("122");

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name = 'clientdevice[pid]']")));
        WebElement pidTextBox = getDriver().findElement(By.xpath("//input[@name = 'clientdevice[pid]']"));
        pidTextBox.clear();
        pidTextBox.sendKeys("888");

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name = 'clientdevice[serialno]']")));
        WebElement serialTextBox = getDriver().findElement(By.xpath("//input[@name = 'clientdevice[serialno]]']"));
        serialTextBox.clear();
        serialTextBox.sendKeys("555");
    }

    public void successPopUp(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class = 'alert alert-success fade in']")));
    }
    public void validateDeviceAdded(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class = 'dataTables_scrollBody']//td[text() = 'Nova 9']")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class = 'dataTables_scrollBody']//td[text() = '122']")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class = 'dataTables_scrollBody']//td[text() = '888']")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class = 'dataTables_scrollBody']//td[text() = '555']")));
    }


}
