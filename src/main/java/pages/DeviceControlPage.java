package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import tools.SeleniumDriver;

import java.util.Arrays;
import java.util.List;

public class DeviceControlPage extends SeleniumDriver {

    public void validateUserLoggedIn() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text() = 'Welcome ']")));
    }
    public void clickControlDeviceButton(){
        if(!wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("maincontenttitle"))).isDisplayed()){
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text() = 'Device Control']")));
            WebElement deviceControlBtn = getDriver().findElement(By.xpath("//span[text() = 'Device Control']"));
            deviceControlBtn.click();
        }
    }

    public void validateSubmenuItem(){
        List<String> list = Arrays.asList("dc_dashboard",
                "clientdevice",
                "clientmachine",
                "client",
                "eppgroup",
                "global_rights",
                "globalsettings",
                "dc_filewhitelist",
                "customcl");
        for (String s : list) {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@data-name='" + s + "']")));
        }
    }
    public void selectSubmenuItem(String option){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text() = '" + option + "']")));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@data-name='clientdevice']")));
        WebElement optionLink = getDriver().findElement(By.xpath("//li[@data-name='clientdevice']"));
        optionLink.click();
    }

    public void selectButtonCreate(){
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn epp-btn create']")));
        WebElement createButton = getDriver().findElement(By.xpath("//button[@class='btn epp-btn create']"));
        createButton.click();
    }

    public void selectButtonSave(){
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn epp-btn']")));
        WebElement createButton = getDriver().findElement(By.xpath("//button[@class='btn epp-btn']"));
        createButton.click();
    }

    public void createPage(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[text() = 'Device Information']")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text() = 'Details']")));
    }

    public void enterDeviceDetails(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name = 'clientdevice[name]']")));
        WebElement userNameTextBox = getDriver().findElement(By.xpath("//input[@name = 'clientdevice[name]']"));
        userNameTextBox.clear();
        userNameTextBox.sendKeys("Nova 9");

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name = 'clientdevice[vid]']")));
        WebElement vidTextBox = getDriver().findElement(By.xpath("//input[@name = 'clientdevice[vid]']"));
        vidTextBox.clear();
        vidTextBox.sendKeys("122");

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name = 'clientdevice[pid]']")));
        WebElement pidTextBox = getDriver().findElement(By.xpath("//input[@name = 'clientdevice[pid]']"));
        pidTextBox.clear();
        pidTextBox.sendKeys("888");

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name = 'clientdevice[serialno]']")));
        WebElement serialTextBox = getDriver().findElement(By.xpath("//input[@name = 'clientdevice[serialno]']"));
        serialTextBox.clear();
        serialTextBox.sendKeys("555");
    }

    public void successPopUp(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class = 'alert alert-success fade in']")));
    }
    public void validateDeviceAdded(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class = 'dataTables_scrollBody']//td[text() = 'Nova 9']")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class = 'dataTables_scrollBody']//td[text() = '122']")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class = 'dataTables_scrollBody']//td[text() = '888']")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class = 'dataTables_scrollBody']//td[text() = '555']")));
    }


}
