package tools;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumDriver {

    private static WebDriver driver;
    public static WebDriverWait wait ;

    public boolean launchDriver() {
                WebDriverManager.chromedriver().setup();
                ChromeOptions capability = new ChromeOptions();
                capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                capability.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS,true);
                driver = new ChromeDriver(capability);
                wait = new WebDriverWait(driver, 60);
        driver.manage().window().maximize();
        return true;
    }

    public WebDriver getDriver() {
        return driver;
    }
    public boolean navigate(String url) {
        try {
            driver.navigate().to(url);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public static boolean shutdown() {
        try {
            driver.close();
            driver.quit();
            return true;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

}
