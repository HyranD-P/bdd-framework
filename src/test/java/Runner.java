import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import entities.Enums;
import io.github.cdimascio.dotenv.Dotenv;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import tools.SeleniumDriver;


@RunWith(Cucumber.class)
@CucumberOptions(features = "src\\test\\java\\features",
        glue = {"step_defs"},
        plugin = {"pretty","html:target/HtmlReports"}
        )
public class Runner extends SeleniumDriver {

        @BeforeClass
        public static void setup(){

                //Instatiate dotenv to read .env file
                Dotenv dotenv = Dotenv.configure().
                        ignoreIfMalformed().
                        load();
                //set credz for site from env.file
                Enums.Users.User_1.username = dotenv.get("ep_username");
                Enums.Users.User_1.password = dotenv.get("ep_password");
        }

        @AfterClass
        public static void breakDown(){
                shutdown();
        }

}
