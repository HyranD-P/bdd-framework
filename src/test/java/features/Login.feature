Feature: EPP Assessment
  This feature logs into the application

  Scenario: Login with correct username and password
    Given I navigate to the EPP site and login
    When I select the Device Control Button on the side menu
    And I validate that the submenu items is shown
    And I select "Devices" option in the submenu
    And I select the create button on the Devices Page
    Then the create page is shown

    When I enter the Device into the required field
    And I select the save button on the Devices Page
    Then the success pop up appears and devices details are shown