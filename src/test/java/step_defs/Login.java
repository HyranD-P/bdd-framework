package step_defs;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import entities.Enums;
import pages.LoginPage;


import java.util.List;

public class Login {
    LoginPage login = new LoginPage("https://events.hosted.endpointprotector.com/index.php/");

    @Given("I navigate to the EPP site and login")
    public void i_navigate_to_the_login_page() {
        login.setUsername(Enums.Users.User_1.username);
        login.setPassword(Enums.Users.User_1.password);
        login.loginButton();
    }



}
