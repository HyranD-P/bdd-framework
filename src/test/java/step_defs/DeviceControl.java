package step_defs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.DeviceControlPage;

public class DeviceControl {
    DeviceControlPage deviceControlPage = new DeviceControlPage();

    @Given("I select the Device Control Button on the side menu")
    public void selectDeviceControl () {
        deviceControlPage.validateUserLoggedIn();
        deviceControlPage.clickControlDeviceButton();
    }
    @And("I validate that the submenu items is shown")
    public void validateSubMenu () {
        deviceControlPage.validateSubmenuItem();
    }

    @And("I select \"([^\"]*)\" option in the submenu$")
    public void selectSubMenuOption (String option) {
        deviceControlPage.selectSubmenuItem(option);
    }
    @And("I select the create button on the Devices Page")
    public void selectCreateButton () {
        deviceControlPage.selectButtonCreate();
    }
    @Then("the create page is shown")
    public void createPage () {
        deviceControlPage.createPage();
    }
    @When("I enter the Device into the required field$")
    public void enterDeviceDetails () {
        deviceControlPage.enterDeviceDetails();
    }
    @And("I select the save button on the Devices Page")
    public void selectSaveButton () {
        deviceControlPage.selectButtonSave();
    }
    @And("the success pop up appears and devices details are shown")
    public void saveValidation () {
        deviceControlPage.successPopUp();
        deviceControlPage.validateDeviceAdded();
    }
}
